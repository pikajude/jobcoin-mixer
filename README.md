# jobcoin-mixer

A useful CLI program that mixes nonexistent cryptocurrency.

## Installation

The easiest way to build and run this project is using `stack`. For installation, see [the Stack readme](https://docs.haskellstack.org/en/stable/README/#how-to-install).

Once you have stack installed, use

```
stack run
```

to run `jobcoin-mixer`.

## Usage

Input a list of addresses to submit a request for them to be used as mixer targets.

## Why Haskell?

I know Haskell's library ecosystem a lot better than Scala's. Specifically I don't know what the correct Scala equivalents are for `ansi-wl-pprint`, `mwc-probability`, `haskeline`, `wreq`, `scientific`, and `cryptonite`.
