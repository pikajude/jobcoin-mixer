module Utils where

import qualified Data.ByteString               as B
import           Data.Time
import           Crypto.Hash
import           Data.String                    ( fromString )

generateAddress :: IO String
generateAddress = do
  t <- fromString . show <$> getCurrentTime
  return $ take 16 $ show (hash (t :: B.ByteString) :: Digest SHA1)
