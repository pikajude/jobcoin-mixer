{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Mixer where

import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Map.Merge.Lazy
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           Control.Monad
import           Control.Concurrent
import           Control.Lens            hiding ( (.=) )
import           Text.PrettyPrint.ANSI.Leijen
                                         hiding ( (<$>) )
import           Control.Monad.IO.Class
import           Network.Wreq.Session           ( Session )
import           Control.Monad.Primitive
import           System.Console.Haskeline
import           Data.Aeson
import           System.Random.MWC.Probability
import           Prelude                 hiding ( log )

import           API
import           API.Types
import           Logging                        ( log_ )
import           Mixer.State

houseAddr :: String
houseAddr = "_HOUSE"

pollIntervalSeconds :: Num p => p
pollIntervalSeconds = 10

getChangedBalances :: [Transaction] -> MixerState -> Map Address Balance
getChangedBalances txns state = M.fromListWith
  (+)
  [ (toAddress t, amount t)
  | t <- txns
  , M.member (toAddress t) (accRequests state)
  ]

updateBalances :: Ord k => Map k Balance -> Map k MixRequest -> Map k MixRequest
updateBalances = merge dropMissing -- drop balances that we're not tracking (shouldn't happen)
                       preserveMissing -- keep unchanged mix requests
                       (zipWithMatched (\_ b mr -> mr & balance_ +~ b))

runMixer
  :: MonadException m => InputT m (String -> Set Address -> IO (), ThreadId)
runMixer = do
  logger <- log_ <$> getExternalPrint

  let log d = logger (dullblue "TRACE" <+> d)

  liftIO $ do
    mixState <- newMVar emptyState

    s        <- newAPISession

    -- should always make sure that the mixState MVar is locked for as little time
    -- as possible, since otherwise it will prevent the UI from processing requests.
    -- we move holding the MVar lock into separate functions, and perform the AI
    -- requests outside those functions
    tid      <- forkIO $ forever $ do
      -- calculate transfers we should make this tick
      mixTransfers <- mixAndSend mixState

      forM_ mixTransfers $ \(addr, bal) -> do
        log
          $  "Transferring "
          <> dullgreen (bold (float (realToFrac bal)))
          <> " coin from house address to "
          <> bold (string addr)
        -- actually publish the new transactions
        -- nice to have: find some sensible way to handle API errors
        runAPI s $ post @_ @Value
          (object
            ["fromAddress" .= houseAddr, "toAddress" .= addr, "amount" .= bal]
          )
          "transactions"

      -- calculate the updated balances of our input addresses (based on public txn log)
      changedBalances <- updateBalancesFromAPI s mixState

      -- whenever someone sends to an input address, move it to house address
      _               <- (`M.traverseWithKey` changedBalances) $ \addr b -> do
        log
          $  "Transferring "
          <> dullgreen (bold (float (realToFrac b)))
          <> " coin from "
          <> bold (string addr)
          <> " to house address"
        runAPI s $ post @_ @Value
          (object
            ["fromAddress" .= addr, "toAddress" .= houseAddr, "amount" .= b]
          )
          "transactions"

      threadDelay (pollIntervalSeconds * 1000000)

    return
        -- return a callback that will add a new request to our mixer state
      ( \ourAddr destAddrs -> modifyMVar_ mixState $ \req ->
        return $ req & reqs_ . at ourAddr ?~ MixRequest
          { reqBalance      = 0
          , reqDestinations = destAddrs
          }
      , tid
      )

updateBalancesFromAPI :: Session -> MVar MixerState -> IO (Map Address Balance)
updateBalancesFromAPI s mixState = do
  Right allTxns <- runAPI s $ get @[Transaction] "transactions"

  state         <- takeMVar mixState

  let
    lastTimestamp = accLastChecked state
    txns          = [ t | t <- allTxns, timestamp t > lastTimestamp ]
    nextTimestamp = case txns of
      [] -> lastTimestamp
      xs -> maximum $ map timestamp xs

    changedBalances = getChangedBalances txns state

    newState =
      state
        &  lastChecked_
        .~ nextTimestamp
        &  reqs_
        %~ updateBalances changedBalances

  putMVar mixState newState

  return changedBalances

mixAndSend :: MVar MixerState -> IO [(Address, Balance)]
mixAndSend mixState = withSystemRandom $ \gen -> do
  state    <- takeMVar mixState

  balances <-
    (`M.traverseMaybeWithKey` accRequests state) $ \_ (MixRequest bal dests) ->
      if bal == 0
        then pure Nothing
        else do
          -- logic, given some input address A with balance B, with N being the number
          -- of destination addresses:
          --
          -- each tick, we want to generate a subtotal of the full balance B,
          -- uniformly divided into N pieces, and send each piece to an output account.
          --
          -- if B <= 2N, we use B as the subtotal. this is because if a breakpoint
          -- isn't chosen, we'll just send smaller and smaller portions of the remaining
          -- balance to the new accounts, but never actually hit the target :D
          let poolSize = fromIntegral (S.size dests)

          fillSize <- case bal of
            n | n <= fromIntegral (poolSize * 2) -> pure $ realToFrac bal
              | otherwise -> sample (uniformR (0, realToFrac bal :: Double)) gen

          bals <- fillN gen poolSize fillSize

          return $ Just $ zip (S.toList dests) bals

  let adjustments  = M.map (negate . sum . map snd) balances
      mixTransfers = concat $ M.elems balances

  putMVar mixState $ state & reqs_ %~ updateBalances adjustments

  return mixTransfers

-- given a target sum T and count S, generate S uniform doubles that sum to T
-- using dirichlet distribution (thanks, stackoverflow!)
fillN
  :: (Real b, Fractional a, PrimMonad m)
  => Gen (PrimState m)
  -> Int
  -> b
  -> m [a]
fillN g s tot'
  | s < 1 = error "fillN called with n < 1"
  | otherwise = do
    ~(_ : ds) <-
      map ((tot *) . realToFrac) <$> sample (dirichlet (replicate s 1)) g
    -- the dirichlet generator always sums to 1, so we need to multiply each factor
    -- by T. however, Double multiplication with small fractions can't be
    -- perfectly accurate, so we cheat a little bit: replace the first item in the list
    -- with (T - sum of the other items), which is generally only a few decimal points away
    return (tot - sum ds : ds)
  where tot = realToFrac tot'
