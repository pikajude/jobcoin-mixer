module Logging where

import           Prelude                 hiding ( log )
import           System.Console.Haskeline
import           Text.PrettyPrint.ANSI.Leijen
import           Control.Monad.IO.Class

-- convenience functions for printing Doc without screwing with the haskeline input

log :: MonadException m => Doc -> InputT m ()
log s = do
  p <- getExternalPrint
  log_ p s

log_ :: MonadIO m => (String -> IO a) -> Doc -> m a
log_ f d = liftIO . f $ displayS (renderPretty 0.4 80 d) ""
