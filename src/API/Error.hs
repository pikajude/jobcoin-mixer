module API.Error where

import           Control.Exception

data APIError
  = BadResponse String -- API sent us back something we can't parse
  deriving (Show, Eq)

instance Exception APIError
