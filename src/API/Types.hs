{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}

module API.Types where

import           Data.Aeson
import           Data.Aeson.TH
import           Data.Scientific
import           Data.Time
import qualified Data.Text.Encoding            as T
import qualified Data.Attoparsec.ByteString.Char8
                                               as A
                                                ( endOfInput
                                                , parseOnly
                                                , scientific
                                                )

type Address = String

-- | newtype wrapper around Scientific. the jobcoin API uses string literals
-- rather than doubles, which is probably the right call. however, Scientific
-- has arbitrary precision :)
newtype Balance = Balance { unBalance :: Scientific }
  deriving newtype (Eq, Num, Ord, Show, Real, Read, Fractional)

instance FromJSON Balance where
  -- parseScientificText, copied from aeson source
  parseJSON =
    withText "balance"
      $ either fail (pure . Balance)
      . A.parseOnly (A.scientific <* A.endOfInput)
      . T.encodeUtf8

instance ToJSON Balance where
  toJSON = toJSON . show

-- datatypes representing Jobcoin API input/output

data AddressInfo = AddressInfo
  { balance :: Balance
  , transactions :: [Transaction]
  } deriving (Show)

data Transaction = Transaction
  { timestamp :: UTCTime
  , fromAddress :: Maybe Address
  , toAddress :: Address
  , amount :: Balance
  }
  deriving Show

deriveJSON defaultOptions ''Transaction
deriveJSON defaultOptions ''AddressInfo
