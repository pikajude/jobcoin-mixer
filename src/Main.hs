{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Main where

import           System.Console.Haskeline
import           Control.Monad.IO.Class
import           Data.List.Split
import           Data.Char                      ( isSpace )
import           Control.Monad.Fix
import           Control.Monad
import qualified Data.Set                      as Set
import           Text.PrettyPrint.ANSI.Leijen
                                         hiding ( (<$>) )
import           Prelude                 hiding ( log )
import           System.Exit

import           API
import           API.Types
import           Mixer
import           Utils
import           Logging

main :: IO ()
main = runInputT defaultSettings $ do
  (newMixRequest, _mixThread) <- runMixer

  forever $ do
    -- use `fix` to create an anonymous loop: this function will continuously
    -- ask for valid input until it actually receives it (see `onFailure`)
    addrInput <- fix $ \f -> do
      l <-
        getInputLine
          "Enter a list of unused Jobcoin addresses to send your mixed Jobcoin to: "
      let onFailure s = log (dullblue "!" <+> s) >> f
          badInput = onFailure "You must enter at least one address."
      case l of
        Nothing ->
          onFailure
            $  "You must enter at least one address. To quit, type the word "
            <> bold "quit"
        Just "quit" -> outputStrLn "Bye!" >> liftIO exitSuccess
        Just addrs  -> case wordsBy (\c -> isSpace c || c == ',') addrs of
          [] -> badInput
          xs -> checkEmpty onFailure xs

    -- unnecessary but nice to have: check that we don't generate a collision
    destAddress <- liftIO generateAddress

    log
      $   dullblue "!"
      <+> "Thanks! You may now send Jobcoin to address "
      <>  bold (dullgreen (string destAddress))
      <>  hardline

    liftIO $ newMixRequest destAddress addrInput

checkEmpty
  :: MonadIO m
  => (Doc -> m (Set.Set Address))
  -> [Address]
  -> m (Set.Set Address)
checkEmpty onFailure addrs = do
  (ok, notOk) <- (`partitionA` addrs) $ \addr -> do
    -- check that all given addresses are unused
    bal <- liftIO (api $ getBalance addr)
    return $ bal == Right 0
  if null notOk
    then pure (Set.fromList ok)
    else
      onFailure
      $   "Some of the addresses you entered appear to be in use:"
      <+> hcat (punctuate (comma <> space) (map (bold . string) notOk))
      <>  ". Please try again"

-- partition lifted to Applicative
partitionA :: Applicative f => (a -> f Bool) -> [a] -> f ([a], [a])
partitionA _ [] = pure ([], [])
partitionA f (x : xs) =
  (\pass (as, bs) -> if pass then (x : as, bs) else (as, x : bs))
    <$> f x
    <*> partitionA f xs
