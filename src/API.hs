{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE ExplicitForAll #-}

module API
  ( module API
  , S.newAPISession
  )
where

import           Control.Monad.IO.Class
import           Control.Monad.Reader
import qualified Network.Wreq.Session          as S
import           Network.Wreq                   ( responseBody )
import           Network.Wreq.Types             ( Postable )
import           Control.Lens
import qualified Data.Aeson                    as A
import           Control.Monad.Except

import           API.Error
import           API.Types

type MonadAPI m = (MonadReader S.Session m, MonadIO m, MonadError APIError m)

get :: forall result m . (MonadAPI m, A.FromJSON result) => String -> m result
get ep = do
  s <- ask
  r <- liftIO $ S.get s $ "http://jobcoin.gemini.com/ravioli/api/" ++ ep
  let bytes = r ^. responseBody
  either (throwError . BadResponse) return $ A.eitherDecode bytes

post
  :: forall body result m
   . (MonadAPI m, A.FromJSON result, Postable body)
  => body
  -> String
  -> m result
post b ep = do
  s <- ask
  r <- liftIO $ S.post s ("http://jobcoin.gemini.com/ravioli/api/" ++ ep) b
  let bytes = r ^. responseBody
  either (throwError . BadResponse) return $ A.eitherDecode bytes

getAddressInfo :: MonadAPI m => Address -> m AddressInfo
getAddressInfo addr = get ("addresses/" ++ addr)

getBalance
  :: (MonadReader S.Session f, MonadIO f, MonadError APIError f)
  => Address
  -> f Balance
getBalance addr = balance <$> getAddressInfo addr

runAPI :: r -> ReaderT r (ExceptT e m) a -> m (Either e a)
runAPI s m = runExceptT $ runReaderT m s

-- for use in ghci
api :: ReaderT S.Session (ExceptT e IO) a -> IO (Either e a)
api m = (`runAPI` m) =<< S.newAPISession
