{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE TemplateHaskell #-}

module Mixer.State where

import           Data.Aeson.TH
import           Data.Map                       ( Map )
import           Data.Set                       ( Set )
import           Data.Time
import           Data.Time.Clock.POSIX
import           Control.Lens

import           API.Types

data MixRequest = MixRequest
  { reqBalance :: !Balance
  , reqDestinations :: !(Set Address) -- invariant: this should never change once created
  } deriving Show

data MixerState = MixerState
  { accRequests :: !(Map Address MixRequest)
    -- since transaction log has no filtering mechanism, we have to filter by time
    -- manually
  , accLastChecked :: !UTCTime
  } deriving Show

emptyState :: MixerState
emptyState = MixerState mempty (posixSecondsToUTCTime 0)

reqs_ :: Lens' MixerState (Map Address MixRequest)
reqs_ = lens accRequests (\ms b -> ms { accRequests = b })
lastChecked_ :: Lens' MixerState UTCTime
lastChecked_ = lens accLastChecked (\ms c -> ms { accLastChecked = c })

balance_ :: Lens' MixRequest Balance
balance_ = lens reqBalance (\r b -> r { reqBalance = b })
dests_ :: Lens' MixRequest (Set Address)
dests_ = lens reqDestinations (\r d -> r { reqDestinations = d })

deriveJSON defaultOptions ''MixRequest
deriveJSON defaultOptions ''MixerState
